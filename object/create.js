// Object.create() method
// on hold

Object.prototype._create = function(object) {
  return object;
}

// implementation
const input = {
  name: "Explore Native",
  description: "Object.create method",
  say: function() {
    return "hello " + this.name;
  }
}
const output = Object._create(input);

console.log(output);